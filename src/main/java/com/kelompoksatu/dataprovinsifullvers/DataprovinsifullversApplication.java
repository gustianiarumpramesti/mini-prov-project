package com.kelompoksatu.dataprovinsifullvers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataprovinsifullversApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataprovinsifullversApplication.class, args);
	}

}
