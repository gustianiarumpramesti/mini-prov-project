package com.kelompoksatu.dataprovinsifullvers.model.entity;

public class Kecamatan {
	private String namaKecamatan;
	private Integer kodeKecamatan;
//	private String kodeKabupaten;

	public Integer getKodeKelurahan() {
		return kodeKecamatan;
	}
	public void setKodeKelurahan(Integer kodeKelurahan) {
		this.kodeKecamatan = kodeKelurahan;
	}
	public String getNamaKelurahan() {
		return namaKecamatan;
	}
	public void setNamaKelurahan(String namaKelurahan) {
		this.namaKecamatan = namaKelurahan;
	}
}
