package com.kelompoksatu.dataprovinsifullvers.model.entity;

public class Kelurahan {
	private Integer kodeKelurahan;
	private String namaKelurahan;
	public Integer getKodeKelurahan() {
		return kodeKelurahan;
	}
	public void setKodeKelurahan(Integer kodeKelurahan) {
		this.kodeKelurahan = kodeKelurahan;
	}
	public String getNamaKelurahan() {
		return namaKelurahan;
	}
	public void setNamaKelurahan(String namaKelurahan) {
		this.namaKelurahan = namaKelurahan;
	}
	

}
