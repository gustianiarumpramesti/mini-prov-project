package com.kelompoksatu.dataprovinsifullvers.controller;

import com.kelompoksatu.dataprovinsifullvers.model.dto.KecamatanDto;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/provinsi/kabupaten/kecamatan")
public class KecamatanController {
	
	// http://localhost:8080/provinsi/kabupaten/kecamatan/
	@GetMapping("/")    
    public List<KecamatanDto> get() {
        return generateDummyKecamatan();
    }

    // http://localhost:8080/provinsi/kabupaten/kecamatan/{id}
    @GetMapping("/{code}")
    public KecamatanDto get(@PathVariable String code) {
        List<KecamatanDto> kecamatanList = generateDummyKecamatan();
        for (KecamatanDto KecamatanDto : kecamatanList) {
            if (KecamatanDto.getKode().equalsIgnoreCase(code)) {
                return KecamatanDto;
            }
        }
        return null;
    }


    public List<KecamatanDto> generateDummyKecamatan() {
        List<KecamatanDto> kecamatanList = new ArrayList<>();

        KecamatanDto cileungsi = new KecamatanDto();
		cileungsi.setKode("32.01.07");
		cileungsi.setProvinsi("Jawa Barat");
		cileungsi.setKabupaten("Bogor");
		cileungsi.setKecamatan("Cileungsi");
		kecamatanList.add(cileungsi);

		KecamatanDto dayeuh = new KecamatanDto();
		dayeuh.setKode("32.01.07");
		dayeuh.setProvinsi("Jawa Barat");
		dayeuh.setKabupaten("Bogor");
		dayeuh.setKecamatan("Cileungsi");
		kecamatanList.add(dayeuh);

		KecamatanDto ciapus = new KecamatanDto();
		ciapus.setKode("32.01.29");
		ciapus.setProvinsi("Jawa Barat");
		ciapus.setKabupaten("Bogor");
		ciapus.setKecamatan("Ciomas");
		kecamatanList.add(ciapus);

        return kecamatanList;
    }
}