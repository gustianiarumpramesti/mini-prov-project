package com.kelompoksatu.dataprovinsifullvers.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelompoksatu.dataprovinsifullvers.model.dto.KelurahanDto;

@RestController
@RequestMapping("/provinsi/kabupaten/kecamatan/kelurahan")
public class KelurahanController {

	@GetMapping("/")
	public List<KelurahanDto> get() {
		return generateDummyKelurahan();
	}

	@GetMapping("/{kode}")
	public KelurahanDto get(@PathVariable String kode) {
		List<KelurahanDto> kelurahanlist = generateDummyKelurahan();
		for (KelurahanDto kelurahanDto : kelurahanlist) {
			if (kelurahanDto.getKode().equalsIgnoreCase(kode)) {
				return kelurahanDto;
			}
		}
		return null;
	}

	public List<KelurahanDto> generateDummyKelurahan() {
		List<KelurahanDto> kelurahanlist = new ArrayList<>();

		KelurahanDto cileungsi = new KelurahanDto();
		cileungsi.setKode("32.01.07.12");
		cileungsi.setProvinsi("Jawa Barat");
		cileungsi.setKabupaten("Bogor");
		cileungsi.setKecamatan("Cileungsi");
		cileungsi.setKelurahan("Cileungsi");
		kelurahanlist.add(cileungsi);

		KelurahanDto dayeuh = new KelurahanDto();
		dayeuh.setKode("32.01.07.04");
		dayeuh.setProvinsi("Jawa Barat");
		dayeuh.setKabupaten("Bogor");
		dayeuh.setKecamatan("Cileungsi");
		dayeuh.setKelurahan("Dayeuh");
		kelurahanlist.add(dayeuh);

		KelurahanDto ciapus = new KelurahanDto();
		ciapus.setKode("32.01.29.08");
		ciapus.setProvinsi("Jawa Barat");
		ciapus.setKabupaten("Bogor");
		ciapus.setKecamatan("Ciomas");
		ciapus.setKelurahan("Ciapus");
		kelurahanlist.add(ciapus);

		KelurahanDto ciomas = new KelurahanDto();
		ciomas.setKode("32.01.29.05");
		ciomas.setProvinsi("Jawa Barat");
		ciomas.setKabupaten("Bogor");
		ciomas.setKecamatan("Ciomas");
		ciomas.setKelurahan("Ciomas");
		kelurahanlist.add(ciomas);

		KelurahanDto bantargadung = new KelurahanDto();
		bantargadung.setKode("32.02.04.01");
		bantargadung.setProvinsi("Jawa Barat");
		bantargadung.setKabupaten("Sukabumi");
		bantargadung.setKecamatan("Bantargadung");
		bantargadung.setKelurahan("Bantargadung");
		kelurahanlist.add(bantargadung);

		KelurahanDto bantargebang = new KelurahanDto();
		bantargebang.setKode("32.02.04.05");
		bantargebang.setProvinsi("Jawa Barat");
		bantargebang.setKabupaten("Sukabumi");
		bantargebang.setKecamatan("Bantargadung");
		bantargebang.setKelurahan("Bantargebang");
		kelurahanlist.add(bantargebang);

		KelurahanDto cikembang = new KelurahanDto();
		cikembang.setKode("32.02.31.08");
		cikembang.setProvinsi("Jawa Barat");
		cikembang.setKabupaten("Sukabumi");
		cikembang.setKecamatan("Caringin");
		cikembang.setKelurahan("Cikembang");
		kelurahanlist.add(cikembang);

		KelurahanDto talaga = new KelurahanDto();
		talaga.setKode("32.02.31.07");
		talaga.setProvinsi("Jawa Barat");
		talaga.setKabupaten("Sukabumi");
		talaga.setKecamatan("Caringin");
		talaga.setKelurahan("Talaga");
		kelurahanlist.add(talaga);

		KelurahanDto wangon = new KelurahanDto();
		wangon.setKode("33.02.02.05");
		wangon.setProvinsi("Jawa Tengah");
		wangon.setKabupaten("Banyumas");
		wangon.setKecamatan("Wangon");
		wangon.setKelurahan("Wangon");
		kelurahanlist.add(wangon);

		KelurahanDto wlahar = new KelurahanDto();
		wlahar.setKode("33.02.02.10");
		wlahar.setProvinsi("Jawa Tengah");
		wlahar.setKabupaten("Banyumas");
		wlahar.setKecamatan("Wangon");
		wlahar.setKelurahan("Wlahar");
		kelurahanlist.add(wlahar);

		KelurahanDto cirahab = new KelurahanDto();
		cirahab.setKode("33.02.01.01");
		cirahab.setProvinsi("Jawa Tengah");
		cirahab.setKabupaten("Banyumas");
		cirahab.setKecamatan("Lumbir");
		cirahab.setKelurahan("Cirahab");
		kelurahanlist.add(cirahab);

		KelurahanDto canduk = new KelurahanDto();
		canduk.setKode("33.02.01.02");
		canduk.setProvinsi("Jawa Tengah");
		canduk.setKabupaten("Banyumas");
		canduk.setKecamatan("Lumbir");
		canduk.setKelurahan("Canduk");
		kelurahanlist.add(canduk);

		KelurahanDto sumberagung = new KelurahanDto();
		sumberagung.setKode("33.12.01.01");
		sumberagung.setProvinsi("Jawa Tengah");
		sumberagung.setKabupaten("Wonogiri");
		sumberagung.setKecamatan("Pracimantoro");
		sumberagung.setKelurahan("Sumberagung");
		kelurahanlist.add(sumberagung);

		KelurahanDto joho = new KelurahanDto();
		joho.setKode("33.12.01.02");
		joho.setProvinsi("Jawa Tengah");
		joho.setKabupaten("Wonogiri");
		joho.setKecamatan("Pracimantoro");
		joho.setKelurahan("Joho");
		kelurahanlist.add(joho);

		KelurahanDto ngargoharjo = new KelurahanDto();
		ngargoharjo.setKode("33.12.02.01");
		ngargoharjo.setProvinsi("Jawa Tengah");
		ngargoharjo.setKabupaten("Wonogiri");
		ngargoharjo.setKecamatan("Giritontro");
		ngargoharjo.setKelurahan("Ngargoharjo");
		kelurahanlist.add(ngargoharjo);

		KelurahanDto tlogosari = new KelurahanDto();
		tlogosari.setKode("33.12.02.02");
		tlogosari.setProvinsi("Jawa Tengah");
		tlogosari.setKabupaten("Wonogiri");
		tlogosari.setKecamatan("Giritontro");
		tlogosari.setKelurahan("Tlogosari");
		kelurahanlist.add(tlogosari);
		KelurahanDto kebraon = new KelurahanDto();
		kebraon.setKode("35.78.01.02");
		kebraon.setProvinsi("Jawa Timur");
		kebraon.setKabupaten("Surabaya");
		kebraon.setKecamatan("Karangpilang");
		kebraon.setKelurahan("Kebraon");
		kelurahanlist.add(kebraon);

		KelurahanDto kedurus = new KelurahanDto();
		kedurus.setKode("35.78.01.03");
		kedurus.setProvinsi("Jawa Timur");
		kedurus.setKabupaten("Surabaya");
		kedurus.setKecamatan("Karangpilang");
		kedurus.setKelurahan("kedurus");
		kelurahanlist.add(kedurus);

		KelurahanDto sidosermo = new KelurahanDto();
		sidosermo.setKode("35.78.02.01");
		sidosermo.setProvinsi("Jawa Timur");
		sidosermo.setKabupaten("Surabaya");
		sidosermo.setKecamatan("Wonocolo");
		sidosermo.setKelurahan("Sidosermo");
		kelurahanlist.add(sidosermo);

		KelurahanDto margorejo = new KelurahanDto();
		margorejo.setKode("35.78.02.03");
		margorejo.setProvinsi("Jawa Timur");
		margorejo.setKabupaten("Surabaya");
		margorejo.setKecamatan("Wonocolo");
		margorejo.setKelurahan("Margorejo");
		kelurahanlist.add(margorejo);

		KelurahanDto sukosari = new KelurahanDto();
		sukosari.setKode("35.77.01.02");
		sukosari.setProvinsi("Jawa Timur");
		sukosari.setKabupaten("Madiun");
		sukosari.setKecamatan("Kartoharjo");
		sukosari.setKelurahan("Sukosari");
		kelurahanlist.add(sukosari);

		KelurahanDto klegen = new KelurahanDto();
		klegen.setKode("35.77.01.03");
		klegen.setProvinsi("Jawa Timur");
		klegen.setKabupaten("Madiun");
		klegen.setKecamatan("Kartoharjo");
		klegen.setKelurahan("Klegen");
		kelurahanlist.add(klegen);

		KelurahanDto sogaten = new KelurahanDto();
		sogaten.setKode("35.77.02.02");
		sogaten.setProvinsi("Jawa Timur");
		sogaten.setKabupaten("Madiun");
		sogaten.setKecamatan("Manguharjo");
		sogaten.setKelurahan("Sogaten");
		kelurahanlist.add(sogaten);

		KelurahanDto patihan = new KelurahanDto();
		patihan.setKode("35.77.02.03");
		patihan.setProvinsi("Jawa Timur");
		patihan.setKabupaten("Madiun");
		patihan.setKecamatan("Manguharjo");
		patihan.setKelurahan("Patihan");
		kelurahanlist.add(patihan);

		return kelurahanlist;
	}
}
