package com.kelompoksatu.dataprovinsifullvers.controller;

import com.kelompoksatu.dataprovinsifullvers.model.dto.ProvinsiDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/provinsi")
public class ProvinsiController {

    // http://localhost:8080/provinsi/

    @GetMapping("/")
    public List<ProvinsiDto> get() {
        return genareteDummyProvinsi();
    }

    // http://localhost:8080/province/33
    @GetMapping("/{code}")
    public ProvinsiDto get(@PathVariable String code) {
        List<ProvinsiDto> provinsiList = genareteDummyProvinsi();
        for (ProvinsiDto provinsiDto : provinsiList) {
            if (provinsiDto.getCode().equalsIgnoreCase(code)) {
                return provinsiDto;
            }
        }
        return null;
    }


    public List<ProvinsiDto> genareteDummyProvinsi() {
        List<ProvinsiDto> provinsiList = new ArrayList<>();

        ProvinsiDto jabar = new ProvinsiDto();
        jabar.setCode("32");
        jabar.setName("Jawa Barat");
        provinsiList.add(jabar);

        ProvinsiDto jateng = new ProvinsiDto();
        jateng.setCode("33");
        jateng.setName("Jawa Tengah");
        provinsiList.add(jateng);
        
        ProvinsiDto jatim = new ProvinsiDto();
        jatim.setCode("35");
        jatim.setName("Jawa Timur");
        provinsiList.add(jatim);

        return provinsiList;
    }
}