package com.kelompoksatu.dataprovinsifullvers.controller;

import com.kelompoksatu.dataprovinsifullvers.model.dto.KabupatenDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/provinsi/kabupaten")
public class KabupatenController {
//  http://localhost:8080/provinsi/kabupaten
	
	@GetMapping("/")
	public List<KabupatenDto> get() {
		return genareteDummyKabupaten();
	}

	@GetMapping("/{kode}")
	public KabupatenDto get(@PathVariable String kode) {
		List<KabupatenDto> kabupatenList = genareteDummyKabupaten();
		for (KabupatenDto kabupatenDto : kabupatenList) {
			if (kabupatenDto.getKode().equalsIgnoreCase(kode)) {
				return kabupatenDto;
			}
		}
		return null;
	}

	public List<KabupatenDto> genareteDummyKabupaten() {
		List<KabupatenDto> kabupatenList = new ArrayList<>();

		KabupatenDto bogor = new KabupatenDto();
		bogor.setKode("32.01");
		bogor.setProvinsi("Jawa Barat");
		bogor.setKabupaten("Bogor");
		kabupatenList.add(bogor);

		KabupatenDto sukabumi = new KabupatenDto();
		sukabumi.setKode("32.02");
		sukabumi.setProvinsi("Jawa Barat");
		sukabumi.setKabupaten("Sukabumi");
		kabupatenList.add(sukabumi);

		KabupatenDto banyumas = new KabupatenDto();
		banyumas.setKode("33.02");
		banyumas.setProvinsi("Jawa Tengah");
		banyumas.setKabupaten("Banyumas");
		kabupatenList.add(banyumas);

		KabupatenDto wonogiri = new KabupatenDto();
		wonogiri.setKode("33.12");
		wonogiri.setProvinsi("Jawa Tengah");
		wonogiri.setKabupaten("Wonogiri");
		kabupatenList.add(wonogiri);

		KabupatenDto surabaya = new KabupatenDto();
		surabaya.setKode("35.78");
		surabaya.setProvinsi("Jawa Timur");
		surabaya.setKabupaten("Surabaya");
		kabupatenList.add(surabaya);

		KabupatenDto madiun = new KabupatenDto();
		madiun.setKode("35.77");
		madiun.setProvinsi("Jawa Timur");
		madiun.setKabupaten("Madiun");
		kabupatenList.add(madiun);

		return kabupatenList;
	}

}
